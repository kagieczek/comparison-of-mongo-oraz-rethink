import sys
import time
from rethinkdb import RethinkDB

r = RethinkDB()

#Connect to subsequent commands
r.connect('localhost', 28015).repl()

#Drop DB
r.db_drop('test_db').run()

#Create DB
r.db_create('test_db').run()

#Create table
r.db('test_db').table_create('test').run()

#Def time
def get_time():
    return time.time()

#Init star-time
start = get_time()

#Get Number of Docs
documentsNumber = int(sys.argv[1])

#Loop
i = 0
while i < documentsNumber:
        r.db("test_db").table("test").insert([{
        "employee": {
            "name": {
                "first_name": "Jan",
                "sure_name": "Nowak"
                },
            "gender": "male",
            "company_id": "123",
            "rank": "captain"
        },
        "company": {
            "id": "064058b6-cea9-a117-b92d-d911027a725a",
            "company": "PZG",
            "type": {
                "industry": "e-commerce",
                "size": 50,
            },
        }
        }]).run()
        i += 1

#Init end-time
end = get_time()-start

print("Sum:")
print(str(end))