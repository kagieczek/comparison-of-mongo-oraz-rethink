import sys
import time
from rethinkdb import RethinkDB

r = RethinkDB()

#Connect to subsequent commands
r.connect('localhost', 28015).repl()

#Def time
def get_time():
    return time.time()

#Init star-time
start = get_time()

#Get Number of Docs
documentsNumber = int(sys.argv[1])

i = 0
while i < documentsNumber:
        docs = list(r.db("test_db").table("test").run())
        count = len(docs)
        r.db("test_db").table("test").filter({name: "Jan"}).update({gender : "male"}).run()
        i += 1

#Init end-time
end = get_time()-start

print("Sum:")
print(str(end))